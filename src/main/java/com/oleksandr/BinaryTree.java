package com.oleksandr;

import java.util.Map;
import java.util.Set;
import java.util.Collection;

public class BinaryTree<K extends Comparable,V> implements Map {
    private Node root;
    private int size;

    public BinaryTree() {
        this.size = 0;
        this.root = null;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        if (size == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean containsKey(Object key) {
        return true;
    }

    public boolean containsValue(Object value) {
        return true;
    }

    public V get(Object value) {
        Node current = root;

    while (current.left != null || current.right != null) {
      if (current.left != null &&
              (current.left.key.compareTo(value.hashCode()) > 0)) {
        current = current.left;
      } else if (current.value == value) {
        return (V) current.value;
      } else {
        current = current.right;
      }
  }

    if (current.value == value) {
      return (V) current.value;
            } else {
        return null;
    }
    }

    public Object put(Object key, Object value) {
        if(isEmpty()) {
            root = new Node((K) key, value);
            this.size++;
            return root;
        }

        Node current = root;

        while (current.left != null || current.right != null) {
            if (current.key.compareTo(value.hashCode()) < 0) {
                if(current.left != null) {
                    current = current.left;
                } else {
                    current.left = new Node((K) key, value);
                    this.size++;
                    return current.left;
                }
            } else if (current.key.compareTo(value.hashCode()) > 0) {
                if(current.right != null) {
                    current = current.right;
                } else {
                    current.right = new Node((K) key, value);
                    this.size++;
                    return current.right;
                }
            } else if (current.key.compareTo(value.hashCode()) == 0) {
                current.value = value;
                return current;
            }
        }
        return current;
    }

    public Object remove(Object key) {
        return null;
    }

  class Node<K extends Comparable, V> {
    private K key;
    private V value;

    private Node left;
    private Node right;

      public Node(K key, V value) {
          this.key = key;
          this.value = value;
          this.left = null;
          this.right = null;
      }

    public V getValue() {
      return value;
    }
  }

        public void putAll(Map m) {}

    public void clear() {

    }

    public Set keySet() {
        return null;
    }

    public Collection values() {
        return null;
    }

    public Set<Entry> entrySet() {
        return null;
    }

}
